<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Data DVD</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Penyewaan DVD</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="data_dvd.php">Data DVD <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="data_cust.php">Data Customer</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Tambah Data DVD</h4>
<form action="proses.php?aksi=insert_dvd" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="kd_dvd">Kode DVD</label>
            <input type="text" placeholder="Masukkan Kode DVD" id="kd_dvd" name="kd_dvd" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="jdl_film">Judul Film</label>
            <input type="text" placeholder="Masukkan Judul Film" id="jdl_film" name="jdl_film" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="kategori">Kategori</label>
            <select class="form-control" name="kategori">            
                <option value="Action">Action</option>
                <option value="Action Crime">Action Crime</option>
                <option value="Horror">Horror</option>
            </select>
        </div>
        <div class="form-group">
            <label for="video">Video</label>
            <input type="text" placeholder="Masukkan Video" id="video" name="video" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="hrg_sewa">Harga Sewa</label>
            <input type="text" placeholder="Masukkan Harga Sewa" id="hrg_sewa" name="hrg_sewa" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="email">Foto</label>
            <input type="file" class="form-control-file" id="file" name="file">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
</form>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>