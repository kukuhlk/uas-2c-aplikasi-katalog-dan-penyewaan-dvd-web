-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Bulan Mei 2020 pada 15.32
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dvd`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `antrian`
--

CREATE TABLE `antrian` (
  `id_cust` varchar(11) NOT NULL,
  `kd_dvd` varchar(11) NOT NULL,
  `lama_sewa` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_cust` int(11) NOT NULL,
  `nm_cust` varchar(50) DEFAULT NULL,
  `jns_identitas` varchar(5) DEFAULT NULL,
  `alamat` varchar(60) DEFAULT NULL,
  `telepon` varchar(13) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_cust`, `nm_cust`, `jns_identitas`, `alamat`, `telepon`, `username`, `password`) VALUES
(1, 'Admin', 'KTP', 'Server', '08', 'admin', 'admin'),
(2, 'Teddy', 'KTP', 'Ngasem', '081', 'teddy', 'ok'),
(3, 'Sansan', 'KTP', 'Semen', '055', 'San', '123'),
(4, 'Tes', 'KTP', 'Server', '081', 'tes', 'tes'),
(5, 'Kukuh', 'KTP', 'Semen', '081242779710', 'kukuhlk', 'kukuh');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_trx`
--

CREATE TABLE `detail_trx` (
  `kd_sewa` varchar(11) NOT NULL,
  `kd_dvd` varchar(11) NOT NULL,
  `lama_sewa` int(3) DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_trx`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `dvd`
--

CREATE TABLE `dvd` (
  `kd_dvd` varchar(11) NOT NULL,
  `jdl_film` varchar(50) DEFAULT NULL,
  `kategori` varchar(20) DEFAULT NULL,
  `photos` varchar(30) DEFAULT NULL,
  `video` varchar(50) DEFAULT NULL,
  `hrg_sewa` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dvd`
--

INSERT INTO `dvd` (`kd_dvd`, `jdl_film`, `kategori`, `photos`, `video`, `hrg_sewa`) VALUES
('D0001', 'Fast & Furious 8', 'Action', 'ff8.jpg', 'https://www.youtube.com/watch?v=uisBaTkQAEs', 25000),
('D0002', 'Fast & Furious 7', 'Action', 'ff7.jpg', 'https://www.youtube.com/watch?v=Skpu5HaVkOc', 25000),
('D0003', 'Iron Man 2', 'Action', 'ironman2.jpg', 'https://www.youtube.com/watch?v=wKtcmiifycU', 30000),
('D0004', 'Iron Man 3', 'Action', 'ironman3.jpg', 'https://www.youtube.com/watch?v=Ke1Y3P9D0Bc', 30000),
('D0005', 'SPIDER-MAN - FAR FROM HOME', 'Action', 'spidermanffh.jpg', 'https://www.youtube.com/watch?v=Nt9L1jCKGnE', 35000),
('D0006', 'Spider-Man- Homecoming', 'Action', 'homecoming.jpg', 'https://www.youtube.com/watch?v=n9DwoQ7HWvI', 35000),
('D0007', 'The Expendables 2', 'Action Crime', 'expen2.jpg', 'https://www.youtube.com/watch?v=ip_CYHdyUBs', 40000),
('D0008', 'The Expendables 3', 'Action Crime', 'expen3.jpg', 'https://www.youtube.com/watch?v=4xD0junWlFc', 40000),
('D0009', 'Transformers - The Era Of Unicron', 'Action', 'tran6.jpg', 'https://www.youtube.com/watch?v=Zaa8bLag5tQ', 50000),
('D0010', 'Transformers - The Last Knight', 'Action', 'tran5.jpg', 'https://www.youtube.com/watch?v=AntcyqJ6brc', 50000);
-- --------------------------------------------------------

--
-- Struktur dari tabel `trx`
--

CREATE TABLE `trx` (
  `kd_sewa` int(11) NOT NULL,
  `tgl_sewa` date DEFAULT NULL,
  `id_cust` varchar(11) NOT NULL,
  `konfirmasi` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trx`
--


--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_cust`);

--
-- Indeks untuk tabel `dvd`
--
ALTER TABLE `dvd`
  ADD PRIMARY KEY (`kd_dvd`);

--
-- Indeks untuk tabel `trx`
--
ALTER TABLE `trx`
  ADD PRIMARY KEY (`kd_sewa`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `customer`
--
ALTER TABLE `customer`
  MODIFY `id_cust` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `trx`
--
ALTER TABLE `trx`
  MODIFY `kd_sewa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
