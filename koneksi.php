<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "db_dvd",
        $con,
        $path = "images/";
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }

        public function tampildvd(){
            $sql = "SELECT m.kd_dvd,m.jdl_film,m.kategori, concat('http://localhost/latihan_web/images/',photos) as url , m.video, m.hrg_sewa
                FROM dvd m";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function insertdvd($kd_dvd,$jdl_film,$kategori,$imstr,$file,$video,$hrg_sewa){
            $jdl_film=mysqli_real_escape_string($this->con,trim($jdl_film));
            $sql = "INSERT into dvd(kd_dvd,jdl_film,kategori,photos,video,hrg_sewa) values(
                '$kd_dvd','$jdl_film','$kategori','$file','$video','$hrg_sewa')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $sql = "delete from dvd where kd_dvd='$kd_dvd'";
                    mysqli_query($conn,$sql);
                    $hasil['respon']="gagal";
                    return $hasil;   
                }else{
                    $hasil['respon']="sukses";
                    return $hasil;
                }
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdvd($kd_dvd){         // fungsi mengambil data
            $sql = "SELECT m.kd_dvd,m.jdl_film,m.kategori,m.photos,concat('http://localhost/latihan_web/images/',photos) as url ,m.video, m.hrg_sewa
                    FROM dvd m
                WHERE m.kd_dvd ='$kd_dvd'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatedvd($kd_dvd,$jdl_film,$kategori,$imstr,$file,$video,$hrg_sewa){ // fungsi mengudapte data
            $jdl_film=mysqli_real_escape_string($this->con,trim($jdl_film));
            if($imstr==""){
                $sql = "UPDATE dvd SET jdl_film='$jdl_film', kategori='$kategori', video='$video', hrg_sewa='$hrg_sewa'
                where kd_dvd='$kd_dvd'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //update data sukses tanpa foto
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }else{
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE dvd SET jdl_film='$jdl_film',kategori='$kategori',photos='$file',video='$video',hrg_sewa='$hrg_sewa'
                            where kd_dvd='$kd_dvd'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }
        }      
        public function deletedvd($kd_dvd){
            $sql = "SELECT photos from dvd where kd_dvd='$kd_dvd'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    unlink($this->path.$photos);
                }
                $sql = "DELETE from dvd where kd_dvd='$kd_dvd'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //delete data sukses
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }
        }

        // DATA CUSTOMER
        public function tampilcust(){
            $sql = "SELECT id_cust, nm_cust, jns_identitas, alamat, telepon, username, password
                FROM customer ";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function insertcust($id_cust,$nm_cust,$jns_identitas,$alamat,$telepon,$username,$password){
            $nm_cust=mysqli_real_escape_string($this->con,trim($nm_cust));
            $sql = "INSERT into customer(id_cust, nm_cust, jns_identitas, alamat, telepon, username, password) values(
                '$id_cust','$nm_cust','$jns_identitas','$alamat','$telepon','$username','$password')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                    $sql = "delete from customer where id_cust='$id_cust'";
                    mysqli_query($conn,$sql);
                    $hasil['respon']="sukses";
                    return $hasil;
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;  
                }
            
        }

        public function editcust($id_cust){         // fungsi mengambil data
            $sql = "SELECT id_cust, nm_cust, jns_identitas, alamat, telepon, username, password
                    FROM customer
                WHERE id_cust ='$id_cust'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatecust($id_cust,$nm_cust,$jns_identitas,$alamat,$telepon,$username,$password){ // fungsi mengudapte data
            $nm_cust=mysqli_real_escape_string($this->con,trim($nm_cust));            
            $sql = "UPDATE customer SET nm_cust='$nm_cust', jns_identitas='$jns_identitas', alamat='$alamat', telepon='$telepon', username='$username', password='$password'
            where id_cust='$id_cust'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                $hasil['respon']="sukses";
                return $hasil; //update data sukses tanpa foto
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }     

        // public function updatecust($id_cust,$nm_cust,$jns_identitas,$alamat,$telepon,$username,$password){
        //     mysqli_query($this->koneksi,"UPDATE customer SET 
        //         SET nm_cust='$nm_cust', jns_identitas='$jns_identitas', alamat='$alamat', telepon='$telepon', username='$username', passowrd='$password'
        //         where id_cust='$id_cust'");
        // }
        
        public function deletecust($id_cust){
                $sql = "DELETE from customer where id_cust='$id_cust'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //delete data sukses
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            
        }
    }
?>