<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data Customer</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Penyewaan DVD Film</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link " href="data_dvd.php">Data DVD </a>
                <a class="nav-item nav-link active" href="data_cust.php">Data Customer<span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Update Data Customer</h4>
<?php foreach($db->editcust($_GET['id_cust']) as $cust) : ?>
<form action="proses.php?aksi=update_cust" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id_cust">ID Customer</label>
            <input type="hidden" id="id_cust" name="id_cust" value="<?= $cust['id_cust'] ?>"><br>
            <?= $cust['id_cust'] ?>
        </div>
        <div class="form-group">
            <label for="nm_cust">Nama Customer</label>
            <input type="text" placeholder="Masukkan Nama Customer" id="nm_cust" name="nm_cust" class="form-control" required value="<?= $cust['nm_cust'] ?>">
        </div>
        <div class="form-group">
            <label for="jns_identitas">Jenis Identitas</label>
            <input type="text" placeholder="Masukkan Jenis Identitas" id="jns_identitas" name="jns_identitas" class="form-control" required value="<?= $cust['jns_identitas'] ?>">
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" placeholder="Masukkan Alamat" id="alamat" name="alamat" class="form-control" required value="<?= $cust['alamat'] ?>">
        </div>
        <div class="form-group">
            <label for="telepon">Telepon</label>
            <input type="text" placeholder="Masukkan Telepon" id="telepon" name="telepon" class="form-control" required value="<?= $cust['telepon'] ?>">
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" placeholder="Masukkan Username" id="username" name="username" class="form-control" required value="<?= $cust['username'] ?>">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" placeholder="Masukkan Password" id="password" name="password" class="form-control" required value="<?= $cust['password'] ?>">
        </div>

        <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
</form>
<?php endforeach ?>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>