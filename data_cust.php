<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Data Customer</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Penyewaan DVD Film</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link " href="data_dvd.php">Data DVD</a>
                <a class="nav-item nav-link active" href="data_cust.php">Data Customer<span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Data Customer</h4>
    <a href="insert_cust.php" class="btn btn-primary mb-3">
      Tambah Data
    </a>
    <?php
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "insertsuccess"){
            $msg = "sukses ditambahkan";
            $tipe = "success";
        }else if($_GET['pesan'] == "updatesuccess"){
            $msg = "sukses diedit";
            $tipe = "success";
        }else if($_GET['pesan'] == "deletesuccess"){
            $msg = "sukses dihapus";
            $tipe = "success";
        }else if($_GET['pesan'] == "insertfailed"){
            $msg = "gagal ditambahkan";
            $tipe = "danger";
        }else if($_GET['pesan'] == "updatefailed"){
            $msg = "gagal diedit";
            $tipe = "danger";
        }else if($_GET['pesan'] == "deletefailed"){
            $msg = "gagal dihapus";
            $tipe = "danger";
        }
        echo '<div class="alert alert-'.$tipe.' alert-dismissible fade show" role="alert">
                Data Customer <strong>'.$msg.'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
    ?>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">ID Customer</th>
        <th scope="col">Nama Customer</th>
        <th scope="col">Jenis Identitas</th>
        <th scope="col">Alamat</th>
        <th scope="col">Telepon</th>
        <th scope="col">Username</th>
        <th scope="col">Passowrd</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($db->tampilcust() as $cust) : ?>
        <tr>
            <td><?= $cust['id_cust'] ?></td>
            <td><?= $cust['nm_cust'] ?></td>
            <td><?= $cust['jns_identitas'] ?></td>
            <td><?= $cust['alamat'] ?></td>
            <td><?= $cust['telepon'] ?></td>
            <td><?= $cust['username'] ?></td>
            <td><?= $cust['password'] ?></td>
            <td>
                <a href="update_cust.php?id_cust=<?php echo $cust['id_cust']; ?>" class="btn btn-warning">Edit</a>
                <a href="proses.php?id_cust=<?php echo $cust['id_cust']; ?>&aksi=delete_cust" class="btn btn-danger">Hapus</a>
			</td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>