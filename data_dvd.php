<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Data DVD</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Penyewaan DVD Film</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="dvd.php">Data DVD <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="data_cust.php">Data Customer</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Data DVD</h4>
    <a href="insert_dvd.php" class="btn btn-primary mb-3">
      Tambah Data
    </a>
    <?php
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "insertsuccess"){
            $msg = "sukses ditambahkan";
            $tipe = "success";
        }else if($_GET['pesan'] == "updatesuccess"){
            $msg = "sukses diedit";
            $tipe = "success";
        }else if($_GET['pesan'] == "deletesuccess"){
            $msg = "sukses dihapus";
            $tipe = "success";
        }else if($_GET['pesan'] == "insertfailed"){
            $msg = "gagal ditambahkan";
            $tipe = "danger";
        }else if($_GET['pesan'] == "updatefailed"){
            $msg = "gagal diedit";
            $tipe = "danger";
        }else if($_GET['pesan'] == "deletefailed"){
            $msg = "gagal dihapus";
            $tipe = "danger";
        }
        echo '<div class="alert alert-'.$tipe.' alert-dismissible fade show" role="alert">
                Data DVD <strong>'.$msg.'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
    ?>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">Kode DVD</th>
        <th scope="col">Judul Film</th>
        <th scope="col">Kategori</th>
        <th scope="col">Foto</th>
        <th scope="col">Video</th>
        <th scope="col">Harga Sewa</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($db->tampildvd() as $dvd) : ?>
        <tr>
            <td><?= $dvd['kd_dvd'] ?></td>
            <td><?= $dvd['jdl_film'] ?></td>
            <td><?= $dvd['kategori'] ?></td>
            <td><img src="<?= $dvd['url'] ?>" width="150px" height="100px" /></td>
            <td><?= $dvd['video'] ?></td>
            <td><?= $dvd['hrg_sewa'] ?></td>
            <td>
                <a href="update_dvd.php?kd_dvd=<?php echo $dvd['kd_dvd']; ?>" class="btn btn-warning">Edit</a>
                <a href="proses.php?kd_dvd=<?php echo $dvd['kd_dvd']; ?>&aksi=delete_dvd" class="btn btn-danger">Hapus</a>
			</td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>