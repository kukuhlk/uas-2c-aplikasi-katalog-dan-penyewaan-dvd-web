<?php
    $DB_NAME = "db_dvd";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nm_cust = $_POST["nm_cust"];
        $sql = "SELECT *
        FROM customer where nm_cust like '%$nm_cust%'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_cust = array();
            while($cust = mysqli_fetch_assoc($result)){
                array_push($data_cust,$cust);
            }
            echo json_encode($data_cust);
        }
    }
?>