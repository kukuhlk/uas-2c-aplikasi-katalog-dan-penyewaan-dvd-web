<?php 
include 'koneksi.php';
$db = new database();
 
$aksi = $_GET['aksi'];
if($aksi == "insert_dvd"){
    $filename = "DC_".date("YmdHis").".".pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION);
    $tempname = $_FILES['file']['tmp_name'];
 	$hasil=$db->insertdvd($_POST['kd_dvd'],$_POST['jdl_film'],$_POST['kategori'],$tempname,$filename,$_POST['video'],$_POST['hrg_sewa']);
    if($hasil['respon']=="sukses"){
        header("location:data_dvd.php?pesan=insertsuccess");
    }else{
        header("location:data_dvd.php?pesan=insertfailed");
    }
}elseif($aksi == "update_dvd"){
    $filename = $_POST['photos'];
    $tempname = $_FILES['file']['tmp_name'];
    $hasil=$db->updatedvd($_POST['kd_dvd'],$_POST['jdl_film'],$_POST['kategori'],$tempname,$filename,$_POST['video'],$_POST['hrg_sewa']);
    if($hasil['respon']=="sukses"){
        header("location:data_dvd.php?pesan=updatesuccess");
    }else{
        header("location:data_dvd.php?pesan=updatefailed");
    }
}elseif($aksi == "delete_dvd"){ 	
    $hasil=$db->deletedvd($_GET['kd_dvd']);
    if($hasil['respon']=="sukses"){
        header("location:data_dvd.php?pesan=deletesuccess");
    }else{
        header("location:data_dvd.php?pesan=deletefailed");
    }
}
//Data customer
elseif($aksi == "insert_cust"){
    // $filename = "DC_".date("YmdHis").".".pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION);
    // $tempname = $_FILES['file']['tmp_name'];
 	$hasil=$db->insertcust("",$_POST['nm_cust'],$_POST['jns_identitas'],$_POST['alamat'],$_POST['telepon'],$_POST['username'],$_POST['password']);
    if($hasil['respon']=="sukses"){
        header("location:data_cust.php?pesan=insertsuccess");
    }else{
        header("location:data_cust.php?pesan=insertfailed");
    }
}elseif($aksi == "update_cust"){
    // $filename = $_POST['photos'];
    // $tempname = $_FILES['file']['tmp_name'];
    $hasil=$db->updatecust($_POST['id_cust'],$_POST['nm_cust'],$_POST['jns_identitas'],$_POST['alamat'],$_POST['telepon'],$_POST['username'],$_POST['password']);
    if($hasil['respon']=="sukses"){
        header("location:data_cust.php?pesan=updatesuccess");
    }else{
        header("location:data_cust.php?pesan=updatefailed");
    }
}elseif($aksi == "delete_cust"){ 	
    $hasil=$db->deletecust($_GET['id_cust']);
    if($hasil['respon']=="sukses"){
        header("location:data_cust.php?pesan=deletesuccess");
    }else{
        header("location:data_cust.php?pesan=deletefailed");
    }
}
?>